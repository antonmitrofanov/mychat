import UIKit
import FirebaseAuth

class Message: NSObject {
    
    var fromId: String?
    var text: String?
    var time: NSNumber?
    var toId: String?
    
    init(dictionary: [String: Any]) {
        self.fromId = dictionary["fromId"] as? String
        self.text = dictionary["text"] as? String
        self.time = dictionary["time"] as? NSNumber
        self.toId = dictionary["toId"] as? String
    }
    
    func chatPartnerId() -> String? {
        if fromId == Auth.auth().currentUser?.uid {
            return toId
        } else {
            return fromId
        }
//        return fromId == Auth.auth().currentUser?.uid ? toId : fromId
    }
    
}
